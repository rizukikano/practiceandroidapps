package com.example.mockup;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.mockup.R;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener {

    EditText editName, editAge, editUsernameRegis, editPasswordRegis;
    Button btnRegister;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        //getActionBar().setDisplayHomeAsUpEnabled(true);
        //getActionBar().setTitle("Register Menu");

        editName = findViewById(R.id.editName);
        editAge = findViewById(R.id.editAge);
        editUsernameRegis = findViewById(R.id.editUsernameRegis);
        editPasswordRegis = findViewById(R.id.editPasswordRegis);
        btnRegister = findViewById(R.id.btnRegister);

        btnRegister.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnRegister:
                Intent intent = new Intent();
                intent.putExtra("MESSAGE", editUsernameRegis.getText().toString());
                setResult(99, intent);
                finish();
                break;
        }
    }
}
